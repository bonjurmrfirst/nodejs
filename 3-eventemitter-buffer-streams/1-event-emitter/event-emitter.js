class EventEmitter {
    #listeners = {};

    addListener(eventName, fn) {
        this.#addListener(eventName, fn, false);

        return this;
    }

    on(eventName, fn) {
        this.#addListener(eventName, fn, false);

        return this;
    }

    removeListener(eventName, fn) {
        if (!this.#listeners[eventName]) {
            return this;
        }

        const listenerIndex = this.#listeners[eventName].findIndex(listener => listener.fn === fn);

        if (listenerIndex !== -1) {
            this.#listeners[eventName].splice(listenerIndex, 1);
        }

        return this;
    }

    off(eventName, fn) {
        this.removeListener(eventName, fn);

        return this;
    }

    once(eventName, fn) {
        this.#addListener(eventName, fn, true);

        return this;
    }

    emit(eventName, ...args) {
        if (!this.#listeners[eventName]) {
            return this;
        }

        this.#listeners[eventName].forEach(listener => listener.fn(...args));

        this.#listeners[eventName]
            .filter(listener => listener.isOnce)
            .map(listener => listener.fn)
            .forEach(fn => this.removeListener(eventName, fn));

        return this;
    }

    listenerCount(eventName) {
        return this.#listeners[eventName]
            ? this.#listeners[eventName].length
            : 0;
    }

    rawListeners(eventName) {
       return this.#listeners[eventName]
            ? [...this.#listeners[eventName]]
            : [];
    }

    #addListener(eventName, fn, isOnce) {
        if (typeof fn !== 'function') {
            throw new Error(`The "listener" argument must be of type function. Received an instance of ${typeof fn}`);
        }

        this.#listeners[eventName]
            ? this.#listeners[eventName].push({ fn, isOnce })
            : this.#listeners[eventName] = [{ fn, isOnce }];
    }
}

module.exports = EventEmitter;
