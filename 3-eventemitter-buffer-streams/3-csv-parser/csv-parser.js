const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

const DIST_FILENAME = 'dist.txt';

function parseCSV(filename) {
    const pathToCSVFile = path.join(__dirname, filename);
    const pathToParsedFile = path.join(__dirname, DIST_FILENAME);

    const writeStream = fs.createWriteStream(pathToParsedFile, { encoding: 'utf-8' });

    // We can rely on csvtojson.fromFile() since it uses createReadStream and reads file line by line under the hood
    csvtojson()
        .fromFile(pathToCSVFile)
        .on('error', error => console.error(`Failed while parsing the file ${filename}, `, error))
        .pipe(writeStream)
        .on('error', error => console.error(`Failed while writing to the file ${filename}, `, error));
}

parseCSV('data.csv');
