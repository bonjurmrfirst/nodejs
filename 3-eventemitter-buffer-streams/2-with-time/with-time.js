const EventEmitter = require('../1-event-emitter/event-emitter');

class WithTime extends EventEmitter {
    #NANOSECONDS_IN_SECOND = 1_000_000_000;

    async execute(asyncFunc, ...args) {
        this.emit('start');

        let  executionTimeSeconds;

        try {
            const startTime = process.hrtime.bigint();
            const data = await asyncFunc(...args);
            executionTimeSeconds = Number(process.hrtime.bigint() - startTime) / this.#NANOSECONDS_IN_SECOND;

            this.emit('data', data);
            this.emit('end');

            console.log(`[WithTime] Function execution took ${executionTimeSeconds} seconds`);
        } catch (error) {
            throw new Error(`[WithTime] Failed executing async function: ${error}`);
        }
    }
}

module.exports = WithTime;
