const https = require('https');
const WithTime = require('./with-time');

const fetchFromUrl = url => {
    return new Promise((resolve, reject) => {
        const request = https.get(url, (response) => {
            let data = '';

            response
                .on('data', (chunk) => data += chunk)
                .on('end', () => {
                    try {
                        resolve(JSON.parse(data));
                    } catch (error) {
                        reject(error);
                    }
                });
        });

        request.on('error', (error) => reject(error));
    });
}


const withTime = new WithTime();

withTime.on('start', () => console.log('About to execute'));
withTime.on('end', () => console.log('Done with execute'));

withTime.execute(fetchFromUrl, 'https://jsonplaceholder.typicode.com/posts/1');

console.log(withTime.rawListeners('end'));
