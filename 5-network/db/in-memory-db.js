class InMemoryDb {
    #users = [];

    addUser(user) {
        this.#users.push({
            ...user,
            hobbies: user.hobbies || []
        });
    }

    getUsers() {
        return this.#users;
    }

    getUser(id) {
        return this.#getUser(id);
    }

    deleteUser(id) {
        this.#users = this.#users.filter(user => user.id !== id);
    }

    getUserHobbies(id) {
        return this.#getUser(id)?.hobbies;
    }

    updateUserHobbies(id, hobbies) {
        const  user = this.#getUser(id);

        if (user) {
            user.hobbies = [...new Set([...user.hobbies, ...hobbies])];
        }
    }

    #getUser(id) {
        return this.#users.find(user => user.id === id);
    }
}

const inMemoryStorage = new InMemoryDb();

module.exports = inMemoryStorage;
