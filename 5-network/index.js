const http = require('http');
const simpleRouter = require('./router/simple-router');

const PORT = 8000;

const server = http.createServer(simpleRouter);

server.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});
