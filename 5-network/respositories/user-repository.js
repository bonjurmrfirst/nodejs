const inMemoryDb = require('../db/in-memory-db');
const { v4: uuidv4 } = require('uuid');

class UserRepository {
    static addUser(user) {
        user.id = uuidv4();
        inMemoryDb.addUser(user);
        return user;
    }

    static getUser(userId) {
        return inMemoryDb.getUser(userId);
    }

    static getUsers() {
        return inMemoryDb.getUsers();
    }

    static deleteUser(userId) {
        inMemoryDb.deleteUser(userId);
    }
}

module.exports = UserRepository;
