const inMemoryDb = require('../db/in-memory-db');

class HobbyRepository {
    static getUserHobbies(userId) {
        return inMemoryDb.getUserHobbies(userId);
    }

    static updateUserHobbies(userId, hobbies) {
        inMemoryDb.updateUserHobbies(userId, hobbies);
    }
}

module.exports = HobbyRepository;
