const {
    postUserController,
    getUsersController,
    deleteUserController
} = require("../controllers/users-controllers");
const {
    getHobbiesController,
    patchHobbiesController
} = require("../controllers/hobbies-controllers");

module.exports = [
    { method: "POST", path: "/api/users", handler: postUserController },
    { method: "GET", path: "/api/users", handler: getUsersController },
    { method: "DELETE", path: "/api/users/:id", handler: deleteUserController },
    { method: "GET", path: "/api/users/:id/hobbies", handler: getHobbiesController },
    { method: "PATCH", path: "/api/users/:id/hobbies", handler: patchHobbiesController }
];
