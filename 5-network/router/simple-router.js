const url = require('url');
const routes = require('./routes');

async function simpleRouter(req, res) {
    const reqUrl = url.parse(req.url);
    const method = req.method;

    const matchRoute = routes.find(route => {
        if (route.method !== method) {
            return;
        }

        const routePaths = route.path.split('/');
        const currentPaths = reqUrl.pathname.split('/');

        if (routePaths.length !== currentPaths.length) {
            return;
        }

        return routePaths.every((routePath, index) =>
            routePath.startsWith(':') || routePath === currentPaths[index]
        );
    });

    if (matchRoute) {
        await matchRoute.handler(req, res);
    } else {
        res.statusCode = 404;
        res.end();
    }
}

module.exports = simpleRouter;
