const url = require('url');
const parseRequestBody = require('../utils/utils');
const UserService = require("../services/user-service");
const { USERS_CACHE_MAX_AGE_SECONDS } = require('../configs/cache.config');

async function postUserController(req, res) {
    const user = await UserService.addUser(await parseRequestBody(req));

    const responseBody = {
        data: [{
            user,
            links: {
                self: `/api/users/${user.id}`,
                hobbies: `/api/users/${user.id}/hobbies`
            }
        }],
        error: null
    };

    res.statusCode = 201;
    res.end(JSON.stringify(responseBody));
}

function getUsersController(req, res) {
    const users = UserService.getUsers().map(user => ({
        ...user,
        links: {
            self: `/api/users/${user.id}`,
            hobbies: `/api/users/${user.id}/hobbies`
        }
    }));

    const responseBody = { data: users, error: null };

    res.setHeader('Cache-Control', `public, max-age=${USERS_CACHE_MAX_AGE_SECONDS}`);
    res.statusCode = 200;
    res.end(JSON.stringify(responseBody));
}

function deleteUserController(req, res) {
    const userId = url.parse(req.url, false).pathname.split('/').pop();

    const isUserDeleted = UserService.deleteUser(userId);

    if (!isUserDeleted){
        res.statusCode = 404;
        res.end(JSON.stringify({ data: null, error: `User with id ${userId} doesn't exist` }));
        return;
    }

    const responseBody = { data: { success: true }, error: null };

    res.statusCode = 200;
    res.end(JSON.stringify(responseBody));
}

module.exports = {
    postUserController,
    getUsersController,
    deleteUserController
}
