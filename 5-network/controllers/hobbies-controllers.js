const url = require('url');
const parseRequestBody = require('../utils/utils');
const HobbyService = require('../services/hobby-service');
const {HOBBIES_CACHE_MAX_AGE_SECONDS} = require('../configs/cache.config');

async function getHobbiesController(req, res) {
    const userId = url.parse(req.url, false).pathname.split('/')[3];
    const hobbiesList = HobbyService.getUserHobbies(userId);

    if (!hobbiesList) {
        res.statusCode = 404;
        res.end(JSON.stringify({ data: null, error: `User with id ${userId} doesn't exist` }));

        return;
    }

    const responseBody = {
        data: {
            hobbies: hobbiesList,
            links: {
                self: `/api/users/${userId}/hobbies`,
                user: `/api/users/${userId}`
            }
        },
        error: null
    };

    res.setHeader('Cache-Control', `public, max-age=${HOBBIES_CACHE_MAX_AGE_SECONDS}`);
    res.statusCode = 200;
    res.end(JSON.stringify(responseBody));
}

async function patchHobbiesController(req, res) {
    const userId = url.parse(req.url, false).pathname.split('/')[3];
    const requestBody = await parseRequestBody(req);

    const user = await HobbyService.updateUserHobbies(userId, requestBody.hobbies);

    if (!user){
        res.statusCode = 404;
        res.end(JSON.stringify({ data: null, error: `User with id ${userId} doesn't exist` }));

        return;
    }

    const responseBody = {
        data: [{
            user,
            links: {
                self: `/api/users/${userId}`,
                hobbies: `/api/users/${userId}/hobbies`
            }
        }],
        error: null
    };

    res.statusCode = 200;
    res.end(JSON.stringify(responseBody));
}

module.exports = {
    getHobbiesController,
    patchHobbiesController
}
