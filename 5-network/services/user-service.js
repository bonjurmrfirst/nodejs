const UserRepository = require('../respositories/user-repository');

class UserService {
    static addUser(user) {
        return UserRepository.addUser(user);
    }

    static getUsers() {
        return UserRepository.getUsers();
    }

    static deleteUser(userId) {
        const user = UserRepository.getUser(userId);

        if (!user) {
            return null;
        }

        UserRepository.deleteUser(userId);

        return true;
    }
}

module.exports = UserService;
