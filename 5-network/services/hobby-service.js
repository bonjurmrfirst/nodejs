const HobbyRepository = require('../respositories/hobby-repository');
const UserRepository = require("../respositories/user-repository");

class HobbyService {
    static getUserHobbies(userId) {
        const user = UserRepository.getUser(userId);

        if (!user) {
            return null;
        }

        return HobbyRepository.getUserHobbies(userId);
    }

    static updateUserHobbies(userId, hobbies) {
        const user = UserRepository.getUser(userId);

        if (!user) {
            return null;
        }

        HobbyRepository.updateUserHobbies(userId, hobbies);

        return user;
    }
}

module.exports = HobbyService;
