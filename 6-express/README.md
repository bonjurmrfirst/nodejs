# Node.js Global Mentoring Program

## Getting started (Local environment)

- Start Podman & MongoDB: <br />
`podman machine start` <br />
`podman compose up`

- Populate database with mock data: <br />
`npm run seed`

- Start application: <br />
`npm start` or `npm run watch`
<hr />
- API is listening on port 3000 <br />
- Mongo Express is listening on port 8081 
