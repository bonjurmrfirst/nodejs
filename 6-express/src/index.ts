import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import { connectDb } from './db/db';
import { cartRouter } from './routes/cart.router';
import productRouter from "./routes/product.router";

const port = 3000;

const app = express();

app.use(bodyParser.json());
app.use(morgan('tiny'));

app.use('/api/profile/cart', cartRouter);
app.use('/api/products', productRouter);

app.set('port', port);

(async () => {
    await connectDb();

    app.listen(app.get('port'), () => console.log(`Server is listening on port ${app.get('port')}`));
})();
