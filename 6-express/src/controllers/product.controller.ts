import type { NextFunction, Request, Response } from 'express';
import { ProductService } from '../services/product.service';

export const getAllProductsController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const products = await ProductService.getAllProducts();

        res.status(200).json({
            data: products,
            error: null
        });

    } catch (error: any) {
        res.status(500).json({ data: null, error: { message: error.message } });
    }
}

export const getProductController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const productId = req.params.productId;
        const product = await ProductService.getProductById(productId);

        res.status(200).json({
            data: product,
            error: null
        });
    } catch (error) {
        res.status(404).json({data: null, error: {message: 'No product with such id'}});

        return;
    }
};
