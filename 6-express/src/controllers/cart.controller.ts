import type { NextFunction, Request, Response } from 'express';
import type { ICart } from '../models/cart.model';
import { CartService } from '../services/cart.service';
import { userIdKey } from '../constants/request-header.constant';
import { ProductService } from '../services/product.service';

export const getCartController = async (req: Request, res: Response, next: NextFunction) => {
    const userId = req.headers[userIdKey] as string;
    const cart = await CartService.getCart(userId);

    const total = cart.items.reduce((sum, item) => {
        return sum + item.product.price * item.count;
    }, 0);

    res.status(200).json({
        data: {
            cart: cart,
            total: total
        },
        error: null
    });
};

export const updateCartController = async (req: Request, res: Response, next: NextFunction) => {
    const userId = req.headers[userIdKey] as string;

    const { productId, count } = req.body;

    const isProductExists = await ProductService.getProductById(productId);

    if (!isProductExists) {
        res.status(200).json({
            data: null,
            error: {
                message: "Products are not valid"
            }
        });

        return;
    }

    const updatedCart: ICart = await CartService.updateCart(userId, productId, count);

    res.status(200).json({
        data: {
            cart: updatedCart,
            total: updatedCart.items.reduce((sum, item) => sum + item.product.price * item.count, 0)
        },
        error: null
    });
};

export const clearCartController = async (req: Request, res: Response, next: NextFunction) => {
    const userId = req.headers[userIdKey] as string;

    const success: boolean = await CartService.clearCart(userId);

    res.status(200).json({
        data: {
            success: success
        },
        error: null
    });
};

export const checkoutCartController = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const userId = req.headers[userIdKey] as string;

        const order = await CartService.checkoutCart(userId);

        res.status(200).json({
            data: {
                order: order
            },
            error: null
        });
    } catch (error: any) {
        if (error.message === 'Cart is empty') {
            res.status(400).json({ data: null, error: { message: error.message } });
        } else {
            next(error);
        }
    }
}
