import { IProduct } from './product.model';
import mongoose from "mongoose";

export interface ICartItem {
    product: IProduct;
    count: number;
}

export interface ICart {
    _id?: string;
    userId: string;
    isDeleted: boolean;
    items: ICartItem[];
}

const cartSchema = new mongoose.Schema({
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    isDeleted: Boolean,
    items: [{
        product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
        count: Number
    }]
});

export const CartModel = mongoose.model('Cart', cartSchema);
