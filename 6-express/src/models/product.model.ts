import mongoose from 'mongoose';

export interface IProduct {
    _id?: string;
    title: string;
    description: string;
    price: number;
}

export const productSchema = new mongoose.Schema({
    title: String,
    description: String,
    price: Number
});

export const ProductModel = mongoose.model('Product', productSchema);
