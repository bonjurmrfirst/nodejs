import mongoose from 'mongoose';

export interface IUser {
    _id?: string;
}

export const userSchema = new mongoose.Schema({
});

export const UserModel = mongoose.model('User', userSchema);

