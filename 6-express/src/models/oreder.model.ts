import type { ICartItem } from './cart.model';
import mongoose from 'mongoose';

type ORDER_STATUS = 'created' | 'completed';

export interface IOrderEntity {
    _id?: string,
    userId: string;
    cartId?: string;
    items: ICartItem[]
    payment: {
        type: string,
        address?: any,
        creditCard?: any,
    },
    delivery: {
        type: string,
        address: any,
    },
    comments: string,
    status: ORDER_STATUS;
    total: number;
}

const orderSchema = new mongoose.Schema({
    userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    cartId: { type: mongoose.Schema.Types.ObjectId, ref: 'Cart' },
    items: [{
        product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
        count: { type: Number }
    }],
    payment: {
        type: { type: String },
        address: String,
        creditCard: String
    },
    delivery: {
        type: { type: String },
        address: { type: String }
    },
    comments: { type: String },
    status: {
        type: String,
        enum: ['created', 'completed'],
        default: 'created'
    },
    total: { type: Number }
});

export const OrderModel = mongoose.model('Order', orderSchema);
