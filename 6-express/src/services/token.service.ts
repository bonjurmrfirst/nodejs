import type { Request, Response, NextFunction } from 'express';
import type { IUser } from '../models/user.model';
import { userIdKey } from '../constants/request-header.constant';
import { UserService } from './user.service';

class TokenService {
    static async validateTokenFromHeader(req: Request, res: Response, next: NextFunction): Promise<void> {
        const userId: string = req.headers[userIdKey] as string;

        if (!userId) {
            res
                .status(401)
                .json({
                    data: null,
                    error: { message: 'User is not authorized' }
                });

            return;
        }

        try {
            const user: IUser | null = await UserService.getUserById(userId);

            if (!user) {
                res.status(403).json({
                    data: null,
                    error: { message: 'You must be authorized user' }
                });

                return;
            }

            req.body.user = user;

            next();
        } catch (error: any) {
            res.status(500).json({ data: null, error: { message: error.message } });

            return;
        }
    }
}

export { TokenService };
