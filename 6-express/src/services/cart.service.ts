import { ICart, ICartItem } from '../models/cart.model';
import { CartRepository } from '../repositories/cart.repository';
import type { IProduct } from '../models/product.model';
import { ProductService } from './product.service';
import type { IOrderEntity } from '../models/oreder.model';
import { OrderService } from './order.service';

class CartService {
    static async getCart(userId: string): Promise<ICart> {
        try {
            let cart: ICart | null = await CartRepository.getCartByUser(userId);

            if (!cart) {
                cart = await CartService._createCart(userId);
            }

            return cart;
        } catch (error) {
            throw error;
        }
    }

    static async updateCart(userId: string, productId: string, count: number): Promise<ICart> {
        try {
            let cart: ICart | null = await CartRepository.getCartByUser(userId);

            if (!cart) {
                cart = await CartRepository.createCart({
                    userId,
                    isDeleted: false,
                    items: []
                });
            }

            const product: IProduct | null = await ProductService.getProductById(productId);

            if (!product) {
                throw new Error('Product not found');
            }

            const productIndex = cart.items.findIndex((item: ICartItem) => item.product._id === productId);

            if (productIndex !== -1) {
                cart.items[productIndex].count = count;
            } else {
                cart.items.push({ product, count });
            }

            cart = await CartRepository.updateCart(cart);

            return cart;
        } catch (error) {
            throw error;
        }
    }

    static async clearCart(userId: string): Promise<boolean> {
        try {
            let cart: ICart | null = await CartRepository.getCartByUser(userId);

            if (!cart) {
                throw new Error('Cart not found');
            }

            cart.items = [];
            await CartRepository.saveCart(cart);

            return true;
        } catch (error) {
            throw error;
        }
    }

    static async checkoutCart(userId: string): Promise<IOrderEntity> {
        let cart: ICart | null = await CartRepository.getCartByUser(userId);

        if (!cart || cart.items.length == 0) {
            throw new Error('Cart is empty');
        }

        let order: IOrderEntity = await OrderService.createOrder(cart, userId);

        return order;
    }

    private static async _createCart(userId: string): Promise<ICart> {
        try {
            const newCart: ICart = {
                userId,
                isDeleted: false,
                items: []
            };

            return await CartRepository.saveCart(newCart);
        } catch (error) {
            throw error;
        }
    }
}

export { CartService };
