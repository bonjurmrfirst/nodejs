import type { IUser } from '../models/user.model';
import { UserRepository } from '../repositories/user.repository';

export class UserService {
    static async getUserById(id: string): Promise<IUser | null> {
        return await UserRepository.getUserById(id);
    }
}
