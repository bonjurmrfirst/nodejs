import { IProduct, ProductModel } from '../models/product.model';

export class ProductService {
    static async getProductById(id: string): Promise<IProduct | null> {
        return ProductModel.findById(id);
    }

    static async getAllProducts(): Promise<IProduct[]> {
        return ProductModel.find();
    }
}
