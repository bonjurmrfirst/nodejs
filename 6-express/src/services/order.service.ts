import type { ICart } from '../models/cart.model';
import type { IOrderEntity } from '../models/oreder.model';
import { OrderRepository } from '../repositories/order.repository';

class OrderService {
    static async createOrder(cart: ICart, userId: string): Promise<IOrderEntity> {
        console.log(cart);
        const total = cart.items.reduce((total, item) => total + (item.product.price * item.count), 0);

        const order: IOrderEntity = {
            userId,
            cartId: cart._id,
            items: cart.items,
            payment: {
                type: 'paypal',
                address: undefined,
                creditCard: undefined
            },
            delivery: {
                type: 'post',
                address: undefined
            },
            comments: '',
            status: 'created',
            total: total
        };

        return await OrderRepository.createOrder(order);
    }
}

export { OrderService };
