import { IOrderEntity, OrderModel } from '../models/oreder.model';

class OrderRepository {
    static async createOrder(order: IOrderEntity): Promise<IOrderEntity> {
        const newOrder = new OrderModel(order);
        await newOrder.save();

        return newOrder.toObject();
    }
}

export { OrderRepository };
