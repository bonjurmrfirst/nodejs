import { CartModel, ICart } from "../models/cart.model";

class CartRepository {
    static async getCartByUser(userId: string): Promise<ICart | null> {
        return CartModel.findOne({ userId: userId }).populate('items.product');
    }

    static async createCart(cart: ICart): Promise<ICart> {
        const newCart = new CartModel(cart);
        await newCart.save();

        return newCart.toObject();
    }

    static async saveCart(cart: ICart): Promise<ICart> {
        console.dir(cart);
        return (await new CartModel(cart).save()).toObject();
    }

    static async updateCart(cart: ICart): Promise<ICart> {
        const updatedCart = await CartModel.findByIdAndUpdate(
            cart._id,
            cart,
            { new: true, useFindAndModify: false }
        )
        if (!updatedCart) {
            throw new Error('No document found with that id.');
        } else {
            return updatedCart.toObject();
        }
    }
}

export { CartRepository };
