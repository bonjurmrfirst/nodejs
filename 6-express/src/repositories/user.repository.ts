import { UserModel, IUser } from '../models/user.model';

class UserRepository {
    static async getUserById(id: string): Promise<IUser | null> {
        return UserModel.findById(id);
    }
}

export { UserRepository };
