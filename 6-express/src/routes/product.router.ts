import { Router } from 'express';
import { getAllProductsController, getProductController } from '../controllers/product.controller';
import {TokenService} from '../services/token.service';

const productRouter: Router = Router();

productRouter.get('/', TokenService.validateTokenFromHeader, getAllProductsController);
productRouter.get('/:productId', TokenService.validateTokenFromHeader, getProductController);

export default productRouter;
