import express from 'express';
import { TokenService } from '../services/token.service';
import {
    checkoutCartController,
    clearCartController,
    getCartController,
    updateCartController
} from '../controllers/cart.controller';

const cartRouter = express.Router();

cartRouter.get('/', TokenService.validateTokenFromHeader, getCartController);
cartRouter.put('/', TokenService.validateTokenFromHeader, updateCartController);
cartRouter.delete('/', TokenService.validateTokenFromHeader, clearCartController);
cartRouter.post('/checkout', TokenService.validateTokenFromHeader, checkoutCartController);


export { cartRouter };
