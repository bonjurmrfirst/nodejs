import mongoose from 'mongoose';

const DB_CONNECTION_STRING = 'mongodb://root:example@127.0.0.1:27017'

const connectDb = async () => {
    try {
        await mongoose.connect(DB_CONNECTION_STRING);
    } catch (error) {
        console.error(error);
    }
}

const closeDbConnection = async () => {
    try {
        await mongoose.connection.close();
    } catch (error) {
        console.error(error);
    }
}

export {
    connectDb,
    closeDbConnection
};
