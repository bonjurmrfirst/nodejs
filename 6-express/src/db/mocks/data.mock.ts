import { IUser } from '../../models/user.model';

export const mockUsers: IUser[] = [{

}];

export const mockProducts = [
    {
        id: 'c6ec8157-6ea6-4a48-a12c-08a8697759a4',
        title: 'MOCK-PRODUCT-1',
        description: 'MOCK-PRODUCT-1 description',
        price: 1
    },
    {
        id: '5bd89c3d-b8d9-4910-806b-6a048dda26b9',
        title: 'MOCK-PRODUCT-1',
        description: 'MOCK-PRODUCT-1 description',
        price: 2
    }
];
