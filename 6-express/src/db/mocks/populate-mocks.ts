import { UserModel } from '../../models/user.model';
import { mockUsers, mockProducts } from "./data.mock";
import { ProductModel } from '../../models/product.model';
import { connectDb, closeDbConnection } from '../db';

const dropCollections = async () => {
    await UserModel.collection.drop();
    await ProductModel.collection.drop();
}

(async () => {
    await connectDb();
    await dropCollections();

    for (const user of mockUsers) {
        const newUser = new UserModel(user);
        await newUser.save();
    }

    for (const product of mockProducts) {
        const newProduct = new ProductModel(product);
        await newProduct.save();
    }

    console.log('✔  Data successfully populated');

    await closeDbConnection();
})();
