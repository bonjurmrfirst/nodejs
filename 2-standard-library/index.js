const { stdoutLogger, fileLogger } = require('./src/loggers');
const getCommand = require('./src/command/command');
const runCommand = require('./src/command/comman-runner');
const { STD_LOG_INTERVAL_MS, FILE_LOG_INTERVAL_MS } = require('./src/constants');

const command = getCommand();
let logTime = Date.now();

setInterval(async () => {
    try {
        let commandResult = await runCommand(command);

        stdoutLogger(commandResult);

        if (Date.now() - logTime >= FILE_LOG_INTERVAL_MS) {
            fileLogger(commandResult);
            logTime = Date.now();
        }
    } catch (error) {
        console.error('Error executing command:', error);
    }
}, STD_LOG_INTERVAL_MS);
