const fs = require('fs');
const path = require('path');
const { LOG_FILENAME } = require('./constants');

const stdoutLogger = (log) => {
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(log.trim());
};

const fileLogger = (log) => {
    const currentTime = Math.floor(Date.now() / 1000);
    const logMessage = `${currentTime} : ${log}`;
    const outputFile = path.join(__dirname, '..', LOG_FILENAME);

    fs.appendFile(
        outputFile,
        logMessage,
        (error) => {
            if (error) {
                console.error('Error logging into file, ', error);
            }
        }
    );
};

module.exports = {
    stdoutLogger,
    fileLogger,
};
