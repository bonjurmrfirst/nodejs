const STD_LOG_INTERVAL_MS = 1000 / 10;
const FILE_LOG_INTERVAL_MS = 1000 * 60;

const LOG_FILENAME = 'activityMonitor.log';

module.exports = {
    STD_LOG_INTERVAL_MS,
    FILE_LOG_INTERVAL_MS,
    LOG_FILENAME
};
