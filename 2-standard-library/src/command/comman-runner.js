const { exec } = require('child_process');

const runCommand = (command) => {
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            if (error || stderr) {
                reject(error || stderr);

                return;
            }

            resolve(stdout);
        });
    });
}

module.exports = runCommand;
