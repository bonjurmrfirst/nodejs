const os = require('os');

const getCommand = () => {
    switch (os.platform()) {
        case 'win32':
            return 'powershell "Get-Process | Sort-Object CPU -Descending | Select-Object -Property Name, CPU, WorkingSet -First 1 | ForEach-Object { $_.Name + \' \' + $_.CPU + \' \' + $_.WorkingSet }"'
        default:
            return 'ps -A -o %cpu,%mem,comm | sort -nr | head -n 1'
    }
}

module.exports = getCommand;
