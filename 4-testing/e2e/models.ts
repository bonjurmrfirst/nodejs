export interface CountryInfo {
    commonName: string;
    officialName: string;
    countryCode: string;
    region: string;
    borders: string[];
}

export interface AvailableCountry {
    countryCode: string;
    name: string;
}
