import request from 'supertest';
import { AvailableCountry } from './models';

const API_BASE_URL = 'https://date.nager.at';

describe('GET /api/v3/CountryInfo/:countryCode', () => {
    const countryCode = 'US';

    it('should return a Country Info object when a valid country code is provided', async () => {
        const response = await request(API_BASE_URL).get(`/api/v3/CountryInfo/${countryCode}`);

        expect(response.statusCode).toBe(200);
        expect(response.body).toEqual({
            commonName: expect.any(String),
            officialName: expect.any(String),
            countryCode: expect.any(String),
            region: expect.any(String),
            borders: expect.any(Array)
        });
    });

    it('should return an error when an invalid country code is provided', async () => {
        const invalidCountryCode = 'INVALID-COUTRY-CODE';
        const response = await request(API_BASE_URL).get(`/api/v3/CountryInfo/${invalidCountryCode}`);

        expect(response.statusCode).toBe(404);
    });
});

describe('GET /api/v3/AvailableCountries', () => {
    it('should return an array of countries', async () => {
        const response = await request(API_BASE_URL).get('/api/v3/AvailableCountries');

        expect(response.statusCode).toBe(200);
        expect(response.body).toBeInstanceOf(Array);

        response.body.forEach((country: AvailableCountry) => {
            expect(country).toEqual({
                countryCode: expect.any(String),
                name: expect.any(String)
            });
        });
    });
});
