import { shortenPublicHoliday, validateInput } from './helpers';
import type { PublicHoliday, PublicHolidayShort } from './types';

describe('shortenPublicHoliday', () => {
    it('should shorten holiday object', () => {
        const mockHoliday: PublicHoliday = {
            date: 'DATE',
            localName: 'LOCAL-NAME',
            name: 'NAME',
            countryCode: 'COUNTRY-CODE',
            fixed: false,
            global: false,
            counties: null,
            launchYear: 1111,
            types: []
        }

        const result = shortenPublicHoliday(mockHoliday);

        const expectedResult: PublicHolidayShort = {
            date: 'DATE',
            localName: 'LOCAL-NAME',
            name: 'NAME'
        };

        expect(result).toEqual(expectedResult);
    });
});

describe('validateInput', () => {
    it('should validate if both year and country are provided', () => {
        const result = validateInput({
            year: new Date().getFullYear(),
            country: 'GB'
        });

        expect(result).toBe(true);
    });

    it('should validate if year only is provided and it is current', () => {
        const result = validateInput({
            year: new Date().getFullYear(),
        });

        expect(result).toBe(true);
    });

    it('should validate if country only is provided and it is supported', () => {
        const result = validateInput({
            country: 'GB'
        });

        expect(result).toBe(true);
    });

    it('should validate if neither year nor country are provided', () => {
        const result = validateInput({});

        expect(result).toBe(true);
    });

    it('should throw an error if the country is not supported', () => {
        const mockCountryCode = 'INVALID-COUNTRY-CODE';

        const result = () => validateInput({
            year: new Date().getFullYear(),
            country: mockCountryCode
        });

        expect(result).toThrow(`Country provided is not supported, received: ${mockCountryCode}`);
    });

    it('should throw an error if the year is not the current year', () => {
        const mockYear = 2023;

        const result = () => validateInput({
            year: mockYear,
            country: 'GB'
        });

        expect(result).toThrow(`Year provided not the current, received: ${mockYear}`);
    });
});

