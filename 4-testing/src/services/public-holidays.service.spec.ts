import axios from 'axios';
import { validateInput } from '../helpers';
import {
    checkIfTodayIsPublicHoliday,
    getListOfPublicHolidays,
    getNextPublicHolidays
} from './public-holidays.service';
import { PUBLIC_HOLIDAYS_API_URL } from '../config';
import { PublicHoliday, PublicHolidayShort } from '../types';

jest.mock('../helpers', () => ({
    validateInput: jest.fn().mockReturnValue(true),
    shortenPublicHoliday: jest.fn().mockImplementation(holiday => ({
        name: holiday.name, localName: holiday.localName, date: holiday.date
    }))
}));

describe('getListOfPublicHolidays', () => {
    it('should call validateInput with correct params', async () => {
        await getListOfPublicHolidays(2024, 'GB');

        expect(validateInput).toHaveBeenCalledWith({ year: 2024, country: 'GB' });
    });

    it('should call axios.get() with correct parameters', async () => {
        jest.spyOn(axios, 'get');

        await getListOfPublicHolidays(2022, 'GB');

        const expectedResult = `${PUBLIC_HOLIDAYS_API_URL}/PublicHolidays/2022/GB`;

        expect(axios.get).toHaveBeenCalledWith(expectedResult);
    });

    it('should return data in correct format', async () => {
        const mockHolidays: PublicHoliday[] = [
            {
                name: 'NAME',
                localName: 'LOCAL-NAME',
                date: 'DATE',
                countryCode: 'GB',
                fixed: false,
                global: false,
                counties: null,
                launchYear: null,
                types: []
            }
        ];

        jest.spyOn(axios, 'get').mockResolvedValue({ data: mockHolidays });

        const result = await getListOfPublicHolidays(3022, 'GB');

        const expectedResult: PublicHolidayShort[] = [
            { name: 'NAME', localName: 'LOCAL-NAME', date: 'DATE' }
        ];

        expect(result).toEqual(expectedResult);
    });

    it('should return an empty array when error happened', async () => {
        jest.spyOn(axios, 'get').mockRejectedValue('ERROR');

        const result = await getListOfPublicHolidays(3024, 'GB');

        expect(result).toEqual([]);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });
});

describe('checkIfTodayIsPublicHoliday', () => {
    it('should call validateInput with correct parameters', async () => {
        await checkIfTodayIsPublicHoliday('GB');

        expect(validateInput).toHaveBeenCalledWith({ country: 'GB' });
    });

    it('should call axios.get() with correct parameters', async () => {
        jest.spyOn(axios, 'get');

        await checkIfTodayIsPublicHoliday('GB');

        const expectedResult = `${PUBLIC_HOLIDAYS_API_URL}/IsTodayPublicHoliday/GB`;

        expect(axios.get).toHaveBeenCalledWith(expectedResult);
    });

    it('should return true if the status is 200', async () => {
        jest.spyOn(axios, 'get').mockResolvedValue({ status: 200 });

        const result = await checkIfTodayIsPublicHoliday('GB');

        expect(result).toBe(true);
    });

    it('should return false if the status is not 200', async () => {
        jest.spyOn(axios, 'get').mockResolvedValue({ status: 403 });

        const result = await checkIfTodayIsPublicHoliday('GB');

        expect(result).toBe(false);
    });

    it('should return false if there is an error', async () => {
        jest.spyOn(axios, 'get').mockRejectedValue('ERROR');

        const result = await checkIfTodayIsPublicHoliday('GB');

        expect(result).toBe(false);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });
});

describe('getNextPublicHolidays', () => {
    it('should call validateInput with correct parameters', async () => {
        await getNextPublicHolidays('GB');

        expect(validateInput).toHaveBeenCalledWith({ country: 'GB' });
    });

    it('should call axios.get() with correct parameters', async () => {
        jest.spyOn(axios, 'get');

        await getNextPublicHolidays('GB');

        const expectedResult = `${PUBLIC_HOLIDAYS_API_URL}/NextPublicHolidays/GB`;

        expect(axios.get).toHaveBeenCalledWith(expectedResult);
    });

    it('should return data in correct format', async () => {
        const mockHolidays: PublicHoliday[] = [
            {
                name: 'NAME',
                localName: 'LOCAL-NAME',
                date: 'DATE',
                countryCode: 'GB',
                fixed: false,
                global: false,
                counties: null,
                launchYear: null,
                types: []
            }
        ];

        jest.spyOn(axios, 'get').mockResolvedValue({ data: mockHolidays });

        const result = await getNextPublicHolidays('GB');

        const expectedResult: PublicHolidayShort[] = [
            { name: 'NAME', localName: 'LOCAL-NAME', date: 'DATE' }
        ];

        expect(result).toEqual(expectedResult);
    });

    it('should return an empty array when an error occurs', async () => {
        jest.spyOn(axios, 'get').mockRejectedValue('Error');

        const result = await getNextPublicHolidays('GB');

        expect(result).toEqual([]);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });
});
