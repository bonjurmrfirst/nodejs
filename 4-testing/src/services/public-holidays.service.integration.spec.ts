import {
    checkIfTodayIsPublicHoliday,
    getListOfPublicHolidays,
    getNextPublicHolidays
} from './public-holidays.service';

describe('getListOfPublicHolidays', () => {
    it('should fetch list of public holidays and format it', async () => {
        const result = await getListOfPublicHolidays(new Date().getFullYear(), 'GB');

        result.forEach(item => {
            expect(item).toEqual({
                name: expect.any(String),
                localName: expect.any(String),
                date: expect.any(String)
            });
        });
    });
});

describe('checkIfTodayIsPublicHoliday', () => {
    it('should fetch data from the API to check if it is holiday today', async () => {
        const result = await checkIfTodayIsPublicHoliday('GB');

        expect(result).toEqual(expect.any(Boolean));
    });
});

describe('getNextPublicHolidays', () => {
    it('should fetch next public holidays and format it', async () => {
        const result = await getNextPublicHolidays('GB');

        result.forEach(item => {
            expect(item).toEqual({
                name: expect.any(String),
                localName: expect.any(String),
                date: expect.any(String)
            });
        });
    });
});
